//require('dotenv').config()
const nodemailer = require('nodemailer')
const moment = require('moment')
const Request = require('tedious').Request
const AUSTRALIA = 'Australia'
const NZ = 'New Zealand'
const USA = 'America'
const {
  AU_CW_DB_USER,
  AU_CW_DB_PASSWORD,
  AU_CW_DB_NAME,
  AU_CW_DB_SERVER,
  NZ_CW_DB_USER,
  NZ_CW_DB_PASSWORD,
  NZ_CW_DB_NAME,
  NZ_CW_DB_SERVER,
  US_CW_DB_USER,
  US_CW_DB_PASSWORD,
  US_CW_DB_NAME,
  US_CW_DB_SERVER,
  TECH_TEAM_EMAIL,
  EMAIL_ADDRESS,
  EMAIL_PASS
} = process.env

const print = (message, country) => console.log(`${moment().format('DD/MM/YYYY hh:mm:ss a')} --- ${message} ${country ? `- ${country}` : ''} --- `)

const executeStatement = (queryString, connection) => {
  return new Promise((resolve, reject) => {
    const request = new Request(queryString, (err) => {
      if (err) {
        console.log({ errror: err })
        reject(err)
      }
    })
    const _rows = []
    request.on('row', columns => {
      const _item = {}
      // Converting the response row to a JSON formatted object: [property]: value

      columns.forEach((column, index) => {
        const name = column.metadata.colName
        _item[name] = columns[index].value
      })
      _rows.push(_item)
    })

    // We return the set of rows after the query is complete, instead of returing row by row
    request.on('doneInProc', () => {
      resolve(_rows)
    })

    request.on('error', (err) => {
      console.log({ err })
    })

    connection.execSql(request)
  })
}

const getCWDBConfig = (country) => {
  // default config === Australia config
  const defaultConfig = {
    userName: AU_CW_DB_USER,
    password: AU_CW_DB_PASSWORD,
    database: AU_CW_DB_NAME,
    server: AU_CW_DB_SERVER
  }

  if (country === NZ) {
    return {
      userName: NZ_CW_DB_USER,
      password: NZ_CW_DB_PASSWORD,
      database: NZ_CW_DB_NAME,
      server: NZ_CW_DB_SERVER
    }
  } else if (country === USA) {
    return {
      userName: US_CW_DB_USER,
      password: US_CW_DB_PASSWORD,
      database: US_CW_DB_NAME,
      server: US_CW_DB_SERVER
    }
  } else {
    return defaultConfig
  }
}

const notifyCompanyNotFound = (org, country, query, cwConfig, queryResult) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    secure: true,
    port: 587,
    service: 'Outlook365',
    auth: {
      user: EMAIL_ADDRESS,
      pass: EMAIL_PASS
    }
  }, {
    from: EMAIL_ADDRESS,
    headers: {
      'X-Laziness-level': 1000
    }
  })

  const message = {
    to: TECH_TEAM_EMAIL, // Comma separated list of recipients
    subject: `${country} Navia Hub - Organisation ${org.name}(${org.orgCode}) not found`, // Subject of the message
    html: `<div>
      Organisation ${org.name}(${org.orgCode}) not found in CW, see details below:
      <div>Query: ${query}</div>
      <div>Result: ${JSON.stringify({ queryResult })}</div>
      <div>Country: ${country}</div>
      <div>Credentials used to connect to cargowise: ${JSON.stringify(cwConfig)}</div>
    </div>`
  }

  return new Promise((resolve, reject) => {
    transporter.sendMail(message, async (error, info) => {
      if (error) {
        console.log(`Error with ${info}:`, error.message)
        reject(error)
      }
      transporter.close()
      resolve()
    })
  })
}

module.exports = {
  print,
  AUSTRALIA,
  NZ,
  USA,
  getCWDBConfig,
  executeStatement,
  notifyCompanyNotFound
}
