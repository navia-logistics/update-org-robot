const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrgSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  orgCode: {
    type: String,
    required: true
  },
  CWOrgId: {
    type: String,
    required: true
  },
  CWOrgDeleted: {
    type: Boolean,
    default: false
  },
  plan: { type: Schema.Types.ObjectId, ref: 'Plans' },
  customPlan: { type: Schema.Types.ObjectId, ref: 'Plans' },
  tracking: {
    type: Boolean,
    required: true,
    default: false
  },
  insights: {
    type: Boolean,
    required: true,
    default: false
  },
  criticalChain: {
    type: Boolean,
    required: true,
    default: false
  },
  attemptsToCheck: {
    type: Number,
    default: 0
  },
  viewAsCA: { type: Schema.Types.ObjectId, ref: 'Users' }
})

module.exports = OrgSchema
