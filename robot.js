const Connection = require('tedious').Connection
const ORG_DB = `SELECT DISTINCT OH_Code, OH_FullName, OH_PK FROM OrgHeader WHERE OH_PK = '<OH_PK>'`
const OrgsModel = require('./OrgModel')
const UsersModel = require('./UserModel')
const util = require('./util')
const { print, executeStatement, getCWDBConfig, notifyCompanyNotFound } = util

module.exports = async (mongoConnection, country) => {
  try {
    const orgModel = mongoConnection.model('Orgs', OrgsModel)
    const userModel = mongoConnection.model('Users', UsersModel)
    print('Getting CW database credentials', country)
    var cwConfig = getCWDBConfig(country)
    const authentication = {
      options: {
        userName: cwConfig.userName,
        password: cwConfig.password
      },
      type: 'default'
    }
    const options = {
      database: cwConfig.database,
      encrypt: false,
      connectTimeout: 2000

    }
    const config = {
      server: cwConfig.server,
      options,
      authentication,
      connectTimeout: 2000
    }

    print('Querying all orgs from mongodb', country)
    const hubOrgs = await orgModel.find({ CWOrgId: { $exists: true }, CWOrgDeleted: false }).exec()
      .catch((err) => {
        print(`Error executing statement hubOrgs: ${err.message}. See the error object below.`, country)
        console.log({ err })
        throw new Error(err)
      })

    print(`Found ${hubOrgs.length} orgs in mongodb`, country)

    if (hubOrgs.length === 0) {
      print('No org with cargowise id... stopping the process', country)
      return
    }

    print(`Let's loop through the organizations and check if they still exist in cargowise`, country)
    print('Connecting to CW', country)

    const promises = hubOrgs.map(async (org) => {

      return new Promise((resolve, reject) => {
        const connection = new Connection(config)
        connection.on('connect', async (err) => {
          if (err) {
            print(`Error connecting to cargowise: ${err.message}. See the error object below.`, country)
            console.log({ err })
            reject(err)
          } else {
            try {
              print('Connected successfully', country)
              print(`Querying the org with id ${org.CWOrgId} from carwise database`, country)
              const query = ORG_DB.replace('<OH_PK>', org.CWOrgId)

              const cwOrgs = await executeStatement(query, connection).catch(err => ({ err }))

              if (cwOrgs.err) {
                console.log(`Error running the query that gets the organization from CW: ${cwConfig.err.message}`)
                console.log(cwOrgs.err)
                reject(cwOrgs.err)

              }

              if (!cwOrgs || cwOrgs.length === 0) {
                print(`Oh no... the organization ${org.name} doesn't exist anymore. Let's block their users and change its flag`, country)
                print(`This is the query executed: ${query}`)
                print(`And this was the result: ${JSON.stringify(cwOrgs)}`)
                delete cwConfig.password
                print(`We used the following credentials to connect to CW: ${JSON.stringify(cwConfig)}`, country)
                const numOfAttempts = org.attemptsToCheck
                print('Lets check if its the first time we dont find this org on CW', country)
                if (!numOfAttempts || (numOfAttempts && numOfAttempts === 0)) {
                  print('It is the first time we dont find this org in CW so lets change increase the number of attempts', country)
                  org.attemptsToCheck = 1
                } else {
                  print(`1) Let's set the flag CWOrgDeleted to true to ${org.name} since the organization doesn't exist anymore.`, country)
                  await notifyCompanyNotFound(org, country, query, cwConfig, cwOrgs)

                  org.CWOrgDeleted = true
                  org.attemptsToCheck = 0
                }

                await org.save().catch((err) => {
                  print(`Error executing statement org.save(): ${err.message}. See the error object below.`, country)
                  console.log({ err })
                  reject(err)
                })

                if (numOfAttempts && numOfAttempts > 0) {
                  print(`1.1)Flag CWOrgDeleted successfully changed to ${org.CWOrgDeleted} for ${org.name}`, country)

                  print(`2) Let's block the users from ${org.name} since the organization doesn't exist anymore.`, country)
                  const users = await userModel.find({ orgCode: org.orgCode }).exec()

                  const userPromises = users.map(user => {
                    user.orgDeleted = true
                    return user.save().catch((err) => {
                      print(`Error executing statement: ${err.message}. See the error object below.`, country)
                      console.log({ err })
                      reject(err)
                    })
                  })
                  print(`2.1) Users from ${org.name} can't login anymore.`, country)

                  print(`All done with ${org.name}.`, country)
                  resolve(userPromises)
                }
              } else {
                print(`The organization ${org.name} still exists in cargowise.`, country)
                print(`Reseting number of attempts`, country)
                org.attemptsToCheck = 0
                await org.save()
                print(`Number of attempts changed to 0 for org ${org.name}`, country)

                print('Closing connection now', country)

                resolve()
              }
              connection.close()
            } catch (err) {
              print('error robot catch', country)
              console.log(err)
              reject(err)
            }
          }
        })
        connection.on('error', (err) => {
          print('error connecting', err.message)
          console.log(err)
          reject(err)
        })
      })
    })

    await Promise.all(promises)
  } catch (err) {
    console.log({ err })
  }
}
