/* eslint-disable no-new */
//require('dotenv').config()
const mongoose = require('mongoose')
const express = require('express')
const http = require('http')
const robot = require('./robot')
var CronJob = require('cron').CronJob
const util = require('./util')
const { print, AUSTRALIA, NZ, USA } = util

const {
  AU_DB,
  NZ_DB,
  US_DB,
  WEBSITES_PORT,
  HOST
} = process.env

const app = express()
const server = http.Server(app)
server.listen(WEBSITES_PORT, HOST)

const run = async () => {
  print('3')
  print('2')
  print('1')
  print('Starting....')
  print('Creating MongoDB Connection', AUSTRALIA)
  const auMongoDBConnection = mongoose.createConnection(AU_DB, { useNewUrlParser: true })
  const nzMongoDBConnection = mongoose.createConnection(NZ_DB, { useNewUrlParser: true })
  const usMongoDBConnection = mongoose.createConnection(US_DB, { useNewUrlParser: true })
  print('Connected with MongoDB', AUSTRALIA)
  print('Australia First', AUSTRALIA)
  await robot(auMongoDBConnection, AUSTRALIA)
  print('Now NZ', NZ)
  await robot(nzMongoDBConnection, NZ)
  print('Finally US', USA)
  await robot(usMongoDBConnection, USA)
  print('Finished... I hope everything went well...')
}

const cron = new CronJob('0 3,15 * * 1-5', run)
// const cron = new CronJob('*/5 * * * *', run)
cron.start()
