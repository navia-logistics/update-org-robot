const mongoose = require('mongoose')
const Schema = mongoose.Schema
const moment = require('moment')

const UserSchema = new Schema({
  fullname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  title: {
    type: String
  },
  orgCode: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: moment.utc()
  },
  favourites: {
    type: Array
  },
  isActive: {
    type: Boolean,
    default: false
  },
  orgDeleted: {
    type: Boolean,
    default: false
  },
  chatKey: {
    type: String,
    default: 'DEFAULT_CHAT_KEY'
  },
  theme: {
    type: String,
    default: 'dark'
  },
  plan: { type: Schema.Types.ObjectId, ref: 'Plans' },
  customPlan: { type: Schema.Types.ObjectId, ref: 'Plans' },
  tracking: {
    type: Boolean,
    required: true,
    default: false
  },
  insights: {
    type: Boolean,
    required: true,
    default: false
  },
  criticalChain: {
    type: Boolean,
    required: true,
    default: false
  },
  isSecret: {
    type: Boolean,
    required: true,
    default: false
  },
  notifications: [{ text: String,
    redirectUrl: String,
    read: Boolean,
    messageId: String,
    createdAt: { type: Date,
      default: moment.utc() },
    shipmentID: String }]
})

module.exports = UserSchema
